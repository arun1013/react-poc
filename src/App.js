import React, {Component} from 'react';
import Contacts from './components/Contacts';
import axios from 'axios';

class App extends Component{


  state = {
    contacts: []
  }

  componentDidMount() {
    //http://jsonplaceholder.typicode.com
    //then(res => res.json())
    //fetch('http://localhost:8082/users')
    //.then(res => res.json())
    axios.get('https://jsonplaceholder.typicode.com/users')
    .then(res => res.data)
    .then((data) => {
      this.setState({ contacts: data })
    })
    .catch(console.log)
  }

  render() {
    return (
      <Contacts key={this.state.contacts.id} contacts={this.state.contacts} />
    );
  }
}
export default App;
