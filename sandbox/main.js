state.contact = require("./data/contactDetails.js").contact;
Sandbox.define('/users', 'GET', function (req, res) {
    res.set('Access-Control-Allow-Origin', '*');
    res.send(state.contact);
});