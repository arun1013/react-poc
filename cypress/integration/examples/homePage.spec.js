context('Actions', () => {
let view;
  if(Cypress.config("videosFolder").includes('mobile'))
  {
   view = 'Mobile';
  }
  else if (Cypress.config("videosFolder").includes('tablet'))
  {
    view = 'Tablet';
  }
  else
  view = 'Desktop;'
  beforeEach(() => {
    cy.server();
    cy.route({
      method: 'GET',
            url: '/users',
            status: 200,
            response: 'fixture:contact.json'
    });
    cy.visit('/')
  })
  it(`table title validation ${view}`, () => {
    cy.get('div#root center h1').contains('Contact List')
  })

  it(`email address validation ${view}`, () => {   
    cy.get('.card-subtitle').contains('Arun@april.biz')
  })
})
